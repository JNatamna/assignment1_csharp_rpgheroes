﻿using RPGHeroes.Hero.Attributes;
using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero;
using RPGHeroes.Hero.Equipment;
using RPGHeroes.Hero.Profession;

namespace RPGHeroes_Test
{
    public class DamageTest
    {
        #region Tests revolving creation and asserting damage with no weapon equipped
        [Fact]
        public void Check_Damage_Weapon_Not_Equipped()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            //  Act
            int totalMageIntelligence = AttributeBuilder.BaseStats(Role.Mage).Intelligence;
            double expected = 1 * (1 + (totalMageIntelligence / 100));
            double actual = mage.Damage();

            //  Assert
            Assert.Equal(expected, actual);
        }
        #endregion
        #region Tests revolving creation and asserting damage with weapon equipped
        [Fact]
        public void Check_Damage_Weapon_Equipped()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            GearCreator Staff = new GearCreator("Staff", 5, Weapon.Staff, 1);
            //  Act
            mage.EquipWeapon(Staff);
            int totalMageIntelligence = AttributeBuilder.BaseStats(Role.Mage).Intelligence;
            double expected = Staff.DamageModifier * (1 + (totalMageIntelligence / 100));
            double actual = mage.Damage();
            //  Assert
            Assert.Equal(expected, actual);
        }
        #endregion
        #region Tests revolving creation and asserting damage with weapon replaced
        [Fact]
        public void Check_Damage_Weapon_Replaced()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            GearCreator Staff = new GearCreator("Staff", 5, Weapon.Staff, 1);
            GearCreator WarStaff = new GearCreator("WarStaff", 10, Weapon.Staff, 1);
            //  Act
            mage.EquipWeapon(Staff);
            mage.EquipWeapon(WarStaff);
            int totalMageIntelligence = AttributeBuilder.BaseStats(Role.Mage).Intelligence;
            double expected = WarStaff.DamageModifier * (1 + (totalMageIntelligence / 100));
            double actual = mage.Damage();
            //  Assert
            Assert.Equal(expected, actual);
        }
        #endregion
        #region Tests revolving creation and asserting damage with weapon replaced
        [Fact]
        public void Check_Damage_Weapon_And_Armor_Equipped()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            GearCreator Staff = new GearCreator("Staff", 5, Weapon.Staff, 1);
            GearCreator ChestPiece = new GearCreator("Rugged chestpiece", new AttributeBuilder(2, 5, 1), Armor.Cloth, Slots.Head, 1);
            //  Act
            mage.EquipWeapon(Staff);
            mage.EquipArmor(ChestPiece);
            int totalMageIntelligence = AttributeBuilder.BaseStats(Role.Mage).Intelligence + ChestPiece.Stats.Intelligence;
            double expected = Staff.DamageModifier * (1 + (totalMageIntelligence / 100));
            double actual = mage.Damage();

            //  Assert
            Assert.Equal(expected, actual);
        }
        #endregion
    }
}
