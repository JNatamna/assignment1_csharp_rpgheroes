﻿using RPGHeroes.Hero.Attributes;
using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero;
using RPGHeroes.Hero.Equipment;
using RPGHeroes.Hero.Profession;

namespace RPGHeroes_Test
{
    public class AttributesTest
    {
        #region Tests revolving creation and asserting attributes with no equipment
        [Fact]
        public void Check_Attributes_No_Equipment()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            int totalMageStr = AttributeBuilder.BaseStats(Role.Mage).Strength;
            int totalMageInt = AttributeBuilder.BaseStats(Role.Mage).Intelligence;
            int totalMageDex = AttributeBuilder.BaseStats(Role.Mage).Dexterity;
            //  Assert
            Assert.Equal(totalMageStr, mage.Stats.Strength);
            Assert.Equal(totalMageInt, mage.Stats.Intelligence);
            Assert.Equal(totalMageDex, mage.Stats.Dexterity);
        }
        #endregion
        #region Tests revolving creation and asserting attributes with one armor equipped
        [Fact]
        public void Check_Attributes_One_Armor_Equipped()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            GearCreator headPiece = new GearCreator("Rugged Chestpiece", new AttributeBuilder(2, 5, 1), Armor.Cloth, Slots.Head, 1);
            int totalMageStr = AttributeBuilder.BaseStats(Role.Mage).Strength + headPiece.Stats.Strength;
            int totalMageInt = AttributeBuilder.BaseStats(Role.Mage).Intelligence + headPiece.Stats.Intelligence;
            int totalMageDex = AttributeBuilder.BaseStats(Role.Mage).Dexterity + headPiece.Stats.Dexterity;
            //  Act
            mage.EquipArmor(headPiece);
            mage.GetTotalAttributes();
            //  Assert
            Assert.Equal(totalMageStr, mage.Stats.Strength);
            Assert.Equal(totalMageInt, mage.Stats.Intelligence);
            Assert.Equal(totalMageDex, mage.Stats.Dexterity);
        }
        #endregion
        #region Tests revolving creation and asserting attributes with two armor equipped
        [Fact]
        public void Check_Attributes_Two_Armor_Equipped()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            GearCreator headPiece = new GearCreator("Rugged Chestpiece", new AttributeBuilder(2, 2, 1), Armor.Cloth, Slots.Head, 1);
            GearCreator bodyPiece = new GearCreator("Rugged Chestpiece", new AttributeBuilder(2, 5, 1), Armor.Cloth, Slots.Body, 1);
            int totalMageStr = AttributeBuilder.BaseStats(Role.Mage).Strength + headPiece.Stats.Strength + bodyPiece.Stats.Strength;
            int totalMageInt = AttributeBuilder.BaseStats(Role.Mage).Intelligence + headPiece.Stats.Intelligence + bodyPiece.Stats.Intelligence;
            int totalMageDex = AttributeBuilder.BaseStats(Role.Mage).Dexterity + headPiece.Stats.Dexterity + bodyPiece.Stats.Dexterity;
            //  Act
            mage.EquipArmor(headPiece);
            mage.EquipArmor(bodyPiece);
            mage.GetTotalAttributes();
            //  Assert
            Assert.Equal(totalMageStr, mage.Stats.Strength);
            Assert.Equal(totalMageInt, mage.Stats.Intelligence);
            Assert.Equal(totalMageDex, mage.Stats.Dexterity);
        }
        #endregion
        #region Tests revolving creation and asserting attributes with one replaced armor
        [Fact]
        public void Check_Attributes_Replaced_Armor_Equipped()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            GearCreator bodyPiece00 = new GearCreator("Rugged Chestpiece", new AttributeBuilder(2, 2, 1), Armor.Cloth, Slots.Body, 1);
            GearCreator bodyPiece01 = new GearCreator("Rugged Chestpiece", new AttributeBuilder(2, 5, 1), Armor.Cloth, Slots.Body, 1);
            int totalMageStr = AttributeBuilder.BaseStats(Role.Mage).Strength + bodyPiece01.Stats.Strength;
            int totalMageInt = AttributeBuilder.BaseStats(Role.Mage).Intelligence + bodyPiece01.Stats.Intelligence;
            int totalMageDex = AttributeBuilder.BaseStats(Role.Mage).Dexterity + bodyPiece01.Stats.Dexterity;
            //  Act
            mage.EquipArmor(bodyPiece00);
            mage.EquipArmor(bodyPiece01);
            mage.GetTotalAttributes();
            //  Assert
            Assert.Equal(totalMageStr, mage.Stats.Strength);
            Assert.Equal(totalMageInt, mage.Stats.Intelligence);
            Assert.Equal(totalMageDex, mage.Stats.Dexterity);
        }
        #endregion
    }
}
