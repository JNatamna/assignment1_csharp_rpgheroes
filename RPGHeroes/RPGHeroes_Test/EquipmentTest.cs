using RPGHeroes.Hero;
using RPGHeroes.Hero.Attributes;
using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero.Equipment;
using RPGHeroes.Hero.Profession;
using Xunit.Abstractions;
using static RPGHeroes.InvalidLogic.Complaint;

namespace RPGHeroes_Test
{
    public class EquipmentTest
    {
        #region Tests revolving creation and asserting name, damage, type, attributes, and level
        [Theory]
        [InlineData("Staff", 1, Weapon.Staff, 1)]
        public void Check_Weapon
            (string name, int damage, Weapon type, int level)
        {
            //  Arrange
            GearCreator weapon = new GearCreator(name, damage, type, level);
            //  Assert
            Assert.Equal("Staff", weapon.Name);
            Assert.Equal(1, weapon.DamageModifier);
            Assert.Equal(Weapon.Staff, weapon.Weapon);
            Assert.Equal(1, weapon.Level);
        }
        [Theory]
        [InlineData("Rugged Cap", 1, 5, 2, Armor.Cloth, Slots.Head, 1)]
        public void Check_Armor
            (string name, int strength, int dexterity, int intelligence, Armor type, Slots slot, int level)
        {
            // Arrange
            AttributeBuilder stats = new AttributeBuilder(strength, dexterity, intelligence);
            GearCreator armor = new GearCreator(name, stats, type, slot, level);
            //  Assert
            Assert.Equal("Rugged Cap", armor.Name);
            Assert.Equal(stats, armor.Stats);
            Assert.Equal(Armor.Cloth, armor.Armor);
            Assert.Equal(Slots.Head, armor.ItemSlot);
            Assert.Equal(1, armor.Level);
        }
        #endregion
        #region Tests revolving creation and asserting equipped items 
        [Theory]
        [InlineData("Staff", 1, Weapon.Staff, 1)]
        public void Check_Equip_Weapon
            (string name, int damage, Weapon type, int level)
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            GearCreator weapon = new GearCreator(name, damage, type, level);
            //  Act
            mage.EquipWeapon(weapon);
            //  Assert
            Assert.Equal(mage.Equipment[Slots.Weapon], weapon);
        }
        [Theory]
        [InlineData("Rugged Cap", 1, 5, 2, Armor.Cloth, Slots.Head, 1)]
        public void Check_Equip_Armor
            (string name, int strength, int dexterity, int intelligence, Armor type, Slots slot, int level)
        {
            // Arrange
            Hero mage = new Mage("Gandalf");
            AttributeBuilder stats = new AttributeBuilder(strength, dexterity, intelligence);
            GearCreator armor = new GearCreator(name, stats, type, slot, level);
            // Act
            mage.EquipArmor(armor);
            //  Assert
            Assert.Equal(mage.Equipment[Slots.Head], armor);
        }
        #endregion
        #region Tests revolving creation and asserting items that can not be equipped
        [Theory]
        [InlineData("Rugged Boots", 1, 3, 2, Armor.Cloth, Slots.Legs, 5)]
        public void Check_Equip_Armor_WrongLevel
            (string name, int strength, int dexterity, int intelligence, Armor type, Slots slot, int level)
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            AttributeBuilder stats = new AttributeBuilder(strength, dexterity, intelligence);
            GearCreator armor = new GearCreator(name, stats, type, slot, level);
            //  Act & Assert
            Assert.Throws<InvalidItemLevelException>(() => { mage.EquipArmor(armor); });
        }
        [Theory]
        [InlineData("Axe", 1, Weapon.Axe, 1)]
        public void Check_Equip_Weapon_WrongClass
            (string name, int damage, Weapon type, int level)
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            GearCreator weapon = new GearCreator(name, damage, type, level);

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => { mage.EquipWeapon(weapon); });
        }
        #endregion
    }
}

