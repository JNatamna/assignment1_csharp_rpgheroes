using RPGHeroes.Hero;
using RPGHeroes.Hero.Attributes;
using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero.Profession;
using Xunit.Abstractions;

namespace RPGHeroes_Test
{
    public class CharacterTest
    {
        #region Tests revolving creation and asserting name
        [Fact]
        public void Check_Correct_Name_Mage()
        {
            //  Arrange 
            Hero mage = new Mage("Gandalf");
            //  Assert
            Assert.Equal("Gandalf", mage.Name);
        }
        [Fact]
        public void Check_Correct_Name_Ranger()
        {
            //  Arrange 
            Hero mage = new Mage("Legolas");
            //  Assert
            Assert.Equal("Legolas", mage.Name);
        }
        [Fact]
        public void Check_Correct_Name_Rogue()
        {
            //  Arrange 
            Hero mage = new Mage("Shadow");
            //  Assert
            Assert.Equal("Shadow", mage.Name);
        }
        [Fact]
        public void Check_Correct_Name_Warrior()
        {
            //  Arrange 
            Hero mage = new Mage("Guts");
            //  Assert
            Assert.Equal("Guts", mage.Name);
        }
        #endregion
        #region Tests revolving creation and asserting class
        [Fact]
        public void Check_Correct_Class_Mage()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            //  Assert
            Assert.IsType<Mage>(mage);
        }
        [Fact]
        public void Check_Correct_Class_Ranger()
        {
            //  Arrange
            Hero ranger = new Ranger("Legolas");
            //  Assert
            Assert.IsType<Ranger>(ranger);
        }
        [Fact]
        public void Check_Correct_Class_Rogue()
        {
            //  Arrange
            Hero rogue = new Mage("Shadow");
            //  Assert
            Assert.IsType<Mage>(rogue);
        }
        [Fact]
        public void Check_Correct_Class_Warrior()
        {
            //  Arrange
            Hero warrior = new Warrior("Guts");
            //  Assert
            Assert.IsType<Warrior>(warrior);
        }
        #endregion
        #region Tests revolving creation and asserting level
        [Fact]
        public void Check_Correct_Level_Mage()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            //  Assert
            Assert.Equal(1, mage.Level);
        }
        [Fact]
        public void Check_Correct_Level_Ranger()
        {
            //  Arrange
            Hero ranger = new Ranger("Legolas");
            //  Assert
            Assert.Equal(1, ranger.Level);
        }
        [Fact]
        public void Check_Correct_Level_Rogue()
        {
            //  Arrange
            Hero rogue = new Rogue("Shadow");
            //  Assert
            Assert.Equal(1, rogue.Level);
        }
        [Fact]
        public void Check_Correct_Level_Warrior()
        {
            //  Arrange
            Hero warrior = new Warrior("Guts");
            //  Assert
            Assert.Equal(1, warrior.Level);
        }
        #endregion
        #region Tests revolving creation and asserting stats
        [Fact]
        public void Check_Correct_Stats_Mage()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            AttributeBuilder atrb = AttributeBuilder.BaseStats(mage.Role);
            //  Assert
            Assert.Equal(mage.Stats.Strength, atrb.Strength);
            Assert.Equal(mage.Stats.Intelligence, atrb.Intelligence);
            Assert.Equal(mage.Stats.Dexterity, atrb.Dexterity);
        }
        [Fact]
        public void Check_Correct_Stats_Ranger()
        {
            //  Arrange
            Hero ranger = new Ranger("Legolas");
            AttributeBuilder atrb = AttributeBuilder.BaseStats(ranger.Role);
            //  Assert
            Assert.Equal(ranger.Stats.Strength, atrb.Strength);
            Assert.Equal(ranger.Stats.Intelligence, atrb.Intelligence);
            Assert.Equal(ranger.Stats.Dexterity, atrb.Dexterity);
        }
        [Fact]
        public void Check_Correct_Stats_Rogue()
        {
            //  Arrange
            Hero rogue = new Rogue("Shadow");
            AttributeBuilder atrb = AttributeBuilder.BaseStats(rogue.Role);
            //  Assert
            Assert.Equal(rogue.Stats.Strength, atrb.Strength);
            Assert.Equal(rogue.Stats.Intelligence, atrb.Intelligence);
            Assert.Equal(rogue.Stats.Dexterity, atrb.Dexterity);
        }
        [Fact]
        public void Check_Correct_Stats_Warrior()
        {
            //  Arrange
            Hero warrior = new Warrior("Guts");
            AttributeBuilder atrb = AttributeBuilder.BaseStats(warrior.Role);
            //  Assert
            Assert.Equal(warrior.Stats.Strength, atrb.Strength);
            Assert.Equal(warrior.Stats.Intelligence, atrb.Intelligence);
            Assert.Equal(warrior.Stats.Dexterity, atrb.Dexterity);
        }
        #endregion
        #region Tests revolving creation and asserting stats after level
        [Fact]
        public void Check_Correct_Stats_After_Level_Mage()
        {
            //  Arrange
            Hero mage = new Mage("Gandalf");
            int atrbStr = AttributeBuilder.LevelUpStats(mage).Strength;
            int atrbInt = AttributeBuilder.LevelUpStats(mage).Intelligence;
            int atrbDex = AttributeBuilder.LevelUpStats(mage).Dexterity;
            // Act
            mage.LevelUp();
            //  Assert
            Assert.Equal(atrbStr, mage.Stats.Strength);
            Assert.Equal(atrbInt, mage.Stats.Intelligence);
            Assert.Equal(atrbDex, mage.Stats.Dexterity);
        }
        [Fact]
        public void Check_Correct_Stats_After_Level_Ranger()
        {
            //  Arrange
            Hero ranger = new Ranger("Legolas");
            int atrbStr = AttributeBuilder.LevelUpStats(ranger).Strength;
            int atrbInt = AttributeBuilder.LevelUpStats(ranger).Intelligence;
            int atrbDex = AttributeBuilder.LevelUpStats(ranger).Dexterity;
            // Act
            ranger.LevelUp();
            //  Assert
            Assert.Equal(atrbStr, ranger.Stats.Strength);
            Assert.Equal(atrbInt, ranger.Stats.Intelligence);
            Assert.Equal(atrbDex, ranger.Stats.Dexterity);
        }
        [Fact]
        public void Check_Correct_Stats_After_Level_Rogue()
        {
            //  Arrange
            Hero rogue = new Mage("Shadow");
            int atrbStr = AttributeBuilder.LevelUpStats(rogue).Strength;
            int atrbInt = AttributeBuilder.LevelUpStats(rogue).Intelligence;
            int atrbDex = AttributeBuilder.LevelUpStats(rogue).Dexterity;
            // Act
            rogue.LevelUp();
            //  Assert
            Assert.Equal(rogue.Stats.Strength, atrbStr);
            Assert.Equal(rogue.Stats.Intelligence, atrbInt);
            Assert.Equal(rogue.Stats.Dexterity, atrbDex);
        }
        [Fact]
        public void Check_Correct_Stats_After_Level_Warrior()
        {
            //  Arrange
            Hero warrior = new Warrior("Guts");
            int atrbStr = AttributeBuilder.LevelUpStats(warrior).Strength;
            int atrbInt = AttributeBuilder.LevelUpStats(warrior).Intelligence;
            int atrbDex = AttributeBuilder.LevelUpStats(warrior).Dexterity;
            // Act
            warrior.LevelUp();
            //  Assert
            Assert.Equal(warrior.Stats.Strength, atrbStr);
            Assert.Equal(warrior.Stats.Intelligence, atrbInt);
            Assert.Equal(warrior.Stats.Dexterity, atrbDex);
        }
        #endregion
    }
}

