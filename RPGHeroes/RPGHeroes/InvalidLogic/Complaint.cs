﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.InvalidLogic
{
    public class Complaint
    {
        public class InvalidWeaponException : Exception
        {
            public string Message => "This weapon can not be used for this class.";
        }

        public class InvalidArmorException : Exception
        {
            public string Message => "This armor can not be used for this class.";
        }

        public class InvalidItemLevelException : Exception
        {
            public string Message => "Level requirement not met.";
        }
        public class InvalidTypeException : Exception
        {
            public string Message => "Level requirement not met.";
        }
    }
}
