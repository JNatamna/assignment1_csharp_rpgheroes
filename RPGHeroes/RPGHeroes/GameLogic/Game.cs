﻿using RPGHeroes.Hero;
using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero.Attributes;
using RPGHeroes.Hero.Equipment;

namespace RPGHeroes.GameLogic
{
    public class Game
    {
        private Hero.Hero? PlayerCharacter { get; set; } = null;
        private readonly Output output = new Output();
        private string CharacterName { get; set; } = "";


        public void Run()
        {
            PickCharacterName();
        }

        private void PickCharacterName()
        {
            output.DisplayInfo($"This is the beginning of your journey brave one.By what name do you wish to be known by?\n(Type your character name and press ENTER)\n");
            Console.CursorVisible = true;
            CharacterName = Console.ReadLine();

            output.DisplayInfo($"Done! From now until the end of the days, the name of your  is {CharacterName}.");
            output.DisplayTip();
            SelectClass();
        }

        private void SelectClass()
        {
            string promt = "This is the beginning of your journey, who are you?";
            string[] options = { "Mage", "Ranger", "Rogue", "Warrior" };
            Output characterCreationMenu = new Output(promt, options);
            int selectedOption = characterCreationMenu.Run();

            PlayerCharacter = selectedOption switch
            {
                0 => HeroCreator.CreateCharacter(CharacterName, Role.Mage),
                1 => HeroCreator.CreateCharacter(CharacterName, Role.Ranger),
                2 => HeroCreator.CreateCharacter(CharacterName, Role.Rogue),
                3 => HeroCreator.CreateCharacter(CharacterName, Role.Warrior)
            };
            output.DisplayInfo("These are your current stats:");
            PlayerCharacter.PrintStats();
            output.DisplayTip();
            GearCreator ChestPiece = new GearCreator("Rugged chestpiece", new AttributeBuilder(0, 0, 5), Armor.Cloth, Slots.Head, 1);
            PlayerCharacter.EquipArmor(ChestPiece);
            PlayerCharacter.Stats = PlayerCharacter.GetTotalAttributes();
            Console.WriteLine("Hello can this line be written"+PlayerCharacter.Stats.Intelligence);
            PlayerCharacter.PrintStats();
        }
    }

}
