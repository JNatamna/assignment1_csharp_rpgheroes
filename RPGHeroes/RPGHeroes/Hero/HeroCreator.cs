﻿using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero.Profession;
using static RPGHeroes.InvalidLogic.Complaint;

namespace RPGHeroes.Hero
{
    public static class HeroCreator
    {
        public static Hero CreateCharacter(string name, Role role)
        {
            return role switch
            {
                Role.Mage => new Mage(name),
                Role.Ranger => new Ranger(name),
                Role.Rogue => new Rogue(name),
                Role.Warrior => new Warrior(name),
                _ => throw new InvalidTypeException()
            };
        }
    }
}
