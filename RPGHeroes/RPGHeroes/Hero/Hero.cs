﻿using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero.Attributes;
using System.Text;
using static RPGHeroes.InvalidLogic.Complaint;
using RPGHeroes.Hero.Equipment;

namespace RPGHeroes.Hero
{
    /// <summary>
    /// Abstract class that represents a hero in a role-playing game.
    /// </summary>
    public abstract class Hero
    {
        public string Name { get; set; } = "";
        public int Level { get; set; } = 1;
        public Role Role { get; init; }
        public AttributeBuilder Stats { get; set; } = new AttributeBuilder();
        public List<Armor> ValidArmorTypes { get; init; } = new List<Armor>();
        public List<Weapon> ValidWeaponTypes { get; init; } = new List<Weapon>();

        // Abstract methods that must be implemented by derived classes
        public abstract void LevelUp();
        public abstract double Damage();
        public abstract AttributeBuilder GetTotalAttributes();

        // Dictionary that represents the equipment slots for the hero
        public Dictionary<Slots, GearCreator?> Equipment { get; } = new()
        {
            {Slots.Weapon, new GearCreator("Unarmed", 1, Weapon.Unarmed, 1)},
            {Slots.Head, null},
            {Slots.Body, null},
            {Slots.Legs, null}
        };

        /// <summary>
        /// Constructor for the Hero class that sets the hero's name.
        /// </summary>
        /// <param name="Name">The hero's name.</param>
        public Hero(string Name)
        {
            this.Name = Name;
        }

        /// <summary>
        /// Equips a weapon for the hero.
        /// </summary>
        /// <param name="weapon">The gear item representing the weapon.</param>
        public void EquipWeapon(GearCreator weapon)
        {
            if (!ValidWeaponTypes.Contains(weapon.Weapon))
            {
                throw new InvalidWeaponException();
            }
            else
            {
                if (Level >= weapon.Level)
                    Equipment[weapon.ItemSlot] = weapon;
                else
                    throw new InvalidItemLevelException();
            }
        }

        /// <summary>
        /// Equips an armor for the hero.
        /// </summary>
        /// <param name="armor">The gear item representing the armor.</param>
        public void EquipArmor(GearCreator armor)
        {
            if (!ValidArmorTypes.Contains(armor.Armor))
            {
                throw new InvalidArmorException();
            }
            else
            {
                if (Level >= armor.Level)
                    Equipment[armor.ItemSlot] = armor;
                else
                    throw new InvalidItemLevelException();
            }
        }

        /// <summary>
        /// Prints the hero's stats to the console.
        /// </summary>
        public virtual void PrintStats()
        {
            StringBuilder stats = new StringBuilder();
            stats.AppendLine("Name: " + Name);
            stats.AppendLine("Level: " + Level);
            stats.AppendLine("Mastery: " + Role);
            stats.AppendLine("Strength: " + Stats.Strength);
            stats.AppendLine("Dexterity: " + Stats.Dexterity);
            stats.AppendLine("Intelligence: " + Stats.Intelligence);
            stats.AppendLine("Damage: " + Damage());
            Console.WriteLine(stats.ToString());
        }
    }
}
