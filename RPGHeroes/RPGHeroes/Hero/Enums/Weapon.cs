﻿
namespace RPGHeroes.Hero.Enums
{
    public enum Weapon
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand,
        Unarmed
    }
}