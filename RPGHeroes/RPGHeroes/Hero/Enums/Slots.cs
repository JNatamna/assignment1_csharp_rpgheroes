﻿
namespace RPGHeroes.Hero.Enums
{
    public enum Slots
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}