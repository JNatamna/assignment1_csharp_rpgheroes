﻿
namespace RPGHeroes.Hero.Enums
{
    public enum Role
    {
        Warrior,
        Mage,
        Rogue,
        Ranger
    }
}