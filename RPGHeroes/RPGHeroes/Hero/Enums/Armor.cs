﻿
namespace RPGHeroes.Hero.Enums
{
    public enum Armor
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}