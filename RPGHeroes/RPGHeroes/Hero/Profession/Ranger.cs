﻿using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero.Attributes;
using RPGHeroes.Hero.Equipment;

namespace RPGHeroes.Hero.Profession
{
    /// <summary>
    /// A class representing a Ranger hero in the RPGHeroes game.
    /// </summary>
    public class Ranger : Hero
    {
        /// <summary>
        /// Initializes a new instance of the Ranger class with the specified name.
        /// </summary>
        /// <param name="name">The name of the ranger.</param>
        public Ranger(string name)
            : base(name)
        {
            Role = Role.Ranger;
            Stats = AttributeBuilder.BaseStats(Role);

            // Add valid armor types for ranger
            ValidArmorTypes.AddRange(new List<Armor>()
            {
                Armor.Leather,
                Armor.Mail
            });
            // Add valid weapon types for ranger
            ValidWeaponTypes.AddRange(new List<Weapon>()
            {
                Weapon.Bow,
            });
        }

        /// <summary>
        /// Increases the level of the ranger hero by 1 and updates its stats accordingly.
        /// </summary>
        public override void LevelUp()
        {
            Level++;
            Stats = AttributeBuilder.LevelUpStats(this);
        }

        /// <summary>
        /// Returns the total attributes of the ranger hero, taking into account the stats from its equipment.
        /// </summary>
        /// <returns>The total attributes of the ranger hero.</returns>
        public override AttributeBuilder GetTotalAttributes()
        {
            foreach (KeyValuePair<Slots, GearCreator?> equipment in Equipment)
            {
                if (equipment.Value != null && equipment.Key != Slots.Weapon)
                {
                    // Add the stats of the equipped armor to the ranger's total stats
                    Stats.Dexterity += equipment.Value.Stats.Dexterity;
                    Stats.Intelligence += equipment.Value.Stats.Intelligence;
                    Stats.Strength += equipment.Value.Stats.Strength;
                }
            }
            return Stats;
        }

        /// <summary>
        /// Calculates and returns the demage dealt by the ranger hero, taking into account its weapon's damage modifier and intelligence stat.
        /// </summary>
        /// <returns>The damage dealt by the ranger hero.</returns>
        public override double Damage() => Equipment[Slots.Weapon]?.DamageModifier * (1 + GetTotalAttributes().Intelligence / 100) ?? 1;
    }
}
