﻿using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero.Attributes;
using RPGHeroes.Hero.Equipment;

namespace RPGHeroes.Hero.Profession
{
    /// <summary>
    /// A class representing a Rogue hero in the RPGHeroes game.
    /// </summary>
    public class Rogue : Hero
    {
        /// <summary>
        /// Initializes a new instance of the Rogue class with the specified name.
        /// </summary>
        /// <param name="name">The name of the rogue.</param>
        public Rogue(string name)
            : base(name)
        {
            Role = Role.Rogue;
            Stats = AttributeBuilder.BaseStats(Role);

            // Add valid armor types for rogue
            ValidArmorTypes.AddRange(new List<Armor>()
            {
                Armor.Leather,
                Armor.Mail
            });
            // Add valid weapon types for rogue
            ValidWeaponTypes.AddRange(new List<Weapon>()
            {
                Weapon.Dagger,
                Weapon.Sword
            });
        }

        /// <summary>
        /// Increases the level of the rogue hero by 1 and updates its stats accordingly.
        /// </summary>
        public override void LevelUp()
        {
            Level++;
            Stats = AttributeBuilder.LevelUpStats(this);
        }

        /// <summary>
        /// Returns the total attributes of the rogue hero, taking into account the stats from its equipment.
        /// </summary>
        /// <returns>The total attributes of the rogue hero.</returns>
        public override AttributeBuilder GetTotalAttributes()
        {
            foreach (KeyValuePair<Slots, GearCreator?> equipment in Equipment)
            {
                if (equipment.Value != null && equipment.Key != Slots.Weapon)
                {
                    // Add the stats of the equipped armor to the rogue's total stats
                    Stats.Dexterity += equipment.Value.Stats.Dexterity;
                    Stats.Intelligence += equipment.Value.Stats.Intelligence;
                    Stats.Strength += equipment.Value.Stats.Strength;
                }
            }
            return Stats;
        }

        /// <summary>
        /// Calculates and returns the darogue dealt by the rogue hero, taking into account its weapon's damage modifier and intelligence stat.
        /// </summary>
        /// <returns>The damage dealt by the rogue hero.</returns>
        public override double Damage() => Equipment[Slots.Weapon]?.DamageModifier * (1 + GetTotalAttributes().Dexterity / 100) ?? 1;
    }
}
