﻿using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero.Attributes;
using RPGHeroes.Hero.Equipment;

namespace RPGHeroes.Hero.Profession
{
    /// <summary>
    /// A class representing a Mage hero in the RPGHeroes game.
    /// </summary>
    public class Mage : Hero
    {
        /// <summary>
        /// Initializes a new instance of the Mage class with the specified name.
        /// </summary>
        /// <param name="name">The name of the mage.</param>
        public Mage(string name)
            : base(name)
        {
            Role = Role.Mage;
            Stats = AttributeBuilder.BaseStats(Role);

            // Add valid armor types for mage
            ValidArmorTypes.AddRange(new List<Armor>()
            {
                Armor.Cloth,
            });

            // Add valid weapon types for mage
            ValidWeaponTypes.AddRange(new List<Weapon>()
            {
                Weapon.Staff,
                Weapon.Wand
            });
        }

        /// <summary>
        /// Increases the level of the mage hero by 1 and updates its stats accordingly.
        /// </summary>
        public override void LevelUp()
        {
            Level++;
            Stats = AttributeBuilder.LevelUpStats(this);
        }

        /// <summary>
        /// Returns the total attributes of the mage hero, taking into account the stats from its equipment.
        /// </summary>
        /// <returns>The total attributes of the mage hero.</returns>
        public override AttributeBuilder GetTotalAttributes()
        {
            foreach (KeyValuePair<Slots, GearCreator?> equipment in Equipment)
            {
                if (equipment.Value != null && equipment.Key != Slots.Weapon)
                {
                    // Add the stats of the equipped armor to the mage's total stats
                    Stats.Dexterity += equipment.Value.Stats.Dexterity;
                    Stats.Intelligence += equipment.Value.Stats.Intelligence;
                    Stats.Strength += equipment.Value.Stats.Strength;
                }
            }
            return Stats;
        }

        /// <summary>
        /// Calculates and returns the damage dealt by the mage hero, taking into account its weapon's damage modifier and intelligence stat.
        /// </summary>
        /// <returns>The damage dealt by the mage hero.</returns>
        public override double Damage() => Equipment[Slots.Weapon]?.DamageModifier * (1 + GetTotalAttributes().Intelligence / 100) ?? 1;
    }
}
