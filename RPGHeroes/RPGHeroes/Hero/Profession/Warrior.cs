﻿using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero.Attributes;
using RPGHeroes.Hero.Equipment;

namespace RPGHeroes.Hero.Profession
{
    /// <summary>
    /// A class representing a Warrior hero in the RPGHeroes game.
    /// </summary>
    public class Warrior : Hero
    {
        /// <summary>
        /// Initializes a new instance of the Warrior class with the specified name.
        /// </summary>
        /// <param name="name">The name of the warrior.</param>
        public Warrior(string name)
            : base(name)
        {
            Role = Role.Warrior;
            Stats = AttributeBuilder.BaseStats(Role);

            // Add valid armor types for warrior
            ValidArmorTypes.AddRange(new List<Armor>()
            {
                Armor.Mail,
                Armor.Plate
            });
            // Add valid weapon types for warrior
            ValidWeaponTypes.AddRange(new List<Weapon>()
            {
                Weapon.Axe,
                Weapon.Hammer,
                Weapon.Sword
            });
        }

        /// <summary>
        /// Increases the level of the warrior hero by 1 and updates its stats accordingly.
        /// </summary>
        public override void LevelUp()
        {
            Level++;
            Stats = AttributeBuilder.LevelUpStats(this);
        }

        /// <summary>
        /// Returns the total attributes of the warrior hero, taking into account the stats from its equipment.
        /// </summary>
        /// <returns>The total attributes of the warrior hero.</returns>
        public override AttributeBuilder GetTotalAttributes()
        {
            foreach (KeyValuePair<Slots, GearCreator?> equipment in Equipment)
            {
                if (equipment.Value != null && equipment.Key != Slots.Weapon)
                {
                    // Add the stats of the equipped armor to the ranger's total stats
                    Stats.Dexterity += equipment.Value.Stats.Dexterity;
                    Stats.Intelligence += equipment.Value.Stats.Intelligence;
                    Stats.Strength += equipment.Value.Stats.Strength;
                }
            }
            return Stats;
        }

        /// <summary>
        /// Calculates and returns the demage dealt by the warrior hero, taking into account its weapon's damage modifier and intelligence stat.
        /// </summary>
        /// <returns>The damage dealt by the warrior hero.</returns>
        public override double Damage() => Equipment[Slots.Weapon]?.DamageModifier * (1 + GetTotalAttributes().Strength / 100) ?? 1;
    }
}
