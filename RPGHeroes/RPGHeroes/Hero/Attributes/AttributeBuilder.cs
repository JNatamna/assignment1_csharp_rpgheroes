﻿using RPGHeroes.Hero.Enums;
using RPGHeroes.Hero.Profession;
using RPGHeroes.InvalidLogic;
using static RPGHeroes.InvalidLogic.Complaint;

namespace RPGHeroes.Hero.Attributes
{
    /// <summary>
    /// Class for building and managing hero attributes.
    /// </summary>
    public class AttributeBuilder
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        /// <summary>
        /// Constructor for AttributeBuilder.
        /// </summary>
        /// <param name="strength">Strength attribute value.</param>
        /// <param name="dexterity">Dexterity attribute value.</param>
        /// <param name="intelligence">Intelligence attribute value.</param>
        public AttributeBuilder(int strength = 0, int dexterity = 0, int intelligence = 0)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        /// <summary>
        /// Get the base stats for a hero based on their role.
        /// </summary>
        /// <param name="role">The hero's role.</param>
        /// <returns>AttributeBuilder containing base stats for the hero.</returns>
        public static AttributeBuilder BaseStats(Role role)
        {
            return role switch
            {
                Role.Mage => new AttributeBuilder { Strength = 1, Dexterity = 1, Intelligence = 8 },
                Role.Ranger => new AttributeBuilder { Strength = 1, Dexterity = 7, Intelligence = 1 },
                Role.Rogue => new AttributeBuilder { Strength = 2, Dexterity = 6, Intelligence = 1 },
                Role.Warrior => new AttributeBuilder { Strength = 5, Dexterity = 2, Intelligence = 1 },
                _ => throw new InvalidTypeException() // Throw exception if role is not valid.
            };
        }

        /// <summary>
        /// Get the stats for a hero after leveling up.
        /// </summary>
        /// <param name="character">The hero that has leveled up.</param>
        /// <returns>AttributeBuilder containing new stats for the hero.</returns>
        public static AttributeBuilder LevelUpStats(Hero character)
        {
            return character switch
            {
                Mage => new AttributeBuilder { Strength = 1 + character.Stats.Strength, Dexterity = 1 + character.Stats.Dexterity, Intelligence = 5 + character.Stats.Intelligence },
                Ranger => new AttributeBuilder { Strength = 1 + character.Stats.Strength, Dexterity = 5 + character.Stats.Dexterity, Intelligence = 1 + character.Stats.Intelligence },
                Rogue => new AttributeBuilder { Strength = 1 + character.Stats.Strength, Dexterity = 4 + character.Stats.Dexterity, Intelligence = 1 + character.Stats.Intelligence },
                Warrior => new AttributeBuilder { Strength = 3 + character.Stats.Strength, Dexterity = 2 + character.Stats.Dexterity, Intelligence = 1 + character.Stats.Intelligence },
                _ => throw new InvalidTypeException() // Throw exception if hero type is not valid.
            };
        }
    }
}
