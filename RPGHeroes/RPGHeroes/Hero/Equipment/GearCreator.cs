﻿using RPGHeroes.Hero.Attributes;
using RPGHeroes.Hero.Enums;

namespace RPGHeroes.Hero.Equipment
{
    public class GearCreator
    {
        public string Name { get; init; } // The name of the gear.
        public int Level { get; init; } // The level requirement for equipping the gear.
        public double DamageModifier { get; init; } // The damage modifier for weapons.
        public AttributeBuilder Stats { get; init; } // The attributes the gear adds to the character.
        public Weapon Weapon { get; init; } // The weapon type for weapons.
        public Armor Armor { get; init; } // The armor type for armor.
        public Slots ItemSlot { get; init; } // The slot where the gear is equipped.

        public GearCreator(string name, AttributeBuilder stats, Armor armorType, Slots slot, int level)
        {
            Name = name;
            Armor = armorType;
            Stats = stats;
            ItemSlot = slot;
            Level = level;
        }

        public GearCreator(string name, double damage, Weapon weaponType, int level)
        {
            Name = name;
            Weapon = weaponType;
            DamageModifier = damage;
            ItemSlot = Slots.Weapon;
            Level = level;
        }
    }
}
