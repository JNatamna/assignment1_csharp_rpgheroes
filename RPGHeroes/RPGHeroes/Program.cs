﻿
using RPGHeroes.GameLogic;

namespace RPGHeroes
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            game.Run();
        }
    }
}
