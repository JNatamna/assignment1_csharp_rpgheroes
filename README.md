# RPG Heroes

RPG Heroes is a console application that allows you to create and customize heroes for a role-playing game. You can create up to 4 different heroes and equip them with different armor and weapons.

## Installation

To install and run this project, follow these steps:

1. Install the .NET SDK on your machine if you haven't already. You can download it from https://dotnet.microsoft.com/download.

2. Clone this repository to your local machine using the following command:

   ```bash
   git clone https://gitlab.com/JNatamna/assignment1_csharp_rpgheroes.git
3. Navigate to the project directory using the following command:

   ```bash
   cd RPGHeroes 

4. Build the project using the following command:

   ```bash
   dotnet build 
5. Test the project by using the following command:

   ```bash
   dotnet test 

## Usage
This project is focused on unit testing and can be run in Visual Studio. To run the tests, follow these steps:

    1. Open the project solution file in Visual Studio.

    2. In the Solution Explorer window, right-click on the project name and select "Run Tests".

The test results will be displayed in the Test Explorer window.

## Contributor
Joachim Noor Atamna